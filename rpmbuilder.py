import os
import subprocess
import logging

import requests
from git import Git, Repo


class RpmBuilder:
    def __init__(self, local_repos=None):
        if not local_repos:
            local_repos = "/tmp/repos"
        self.local_repos = local_repos

    def get_local_repo_path(self, owner: str, project: str) -> str:
        added_path = f"{owner}/{project}"
        return os.path.join(self.local_repos, added_path)

    def checkout_latest_tag(self, owner: str, project: str, tag: str) -> str:
        repo = f"https://github.com/{owner}/{project}.git"

        local_repo_path = self.get_local_repo_path(owner, project)
        if os.path.isdir(local_repo_path):
            logging.info("Checking out tag %s for %s/%s", tag, owner, project)
            g = Git(local_repo_path)
            g.checkout(tag)
        else:
            logging.info("Cloning tag %s for %s/%s", tag, owner, project)
            Repo.clone_from(repo, local_repo_path, branch=tag)

        return local_repo_path

    def get_latest_tag(self, owner: str, project: str) -> str:
        url = f"https://api.github.com/repos/{owner}/{project}/releases/latest"
        response = requests.get(url)
        if response.ok:
            data = response.json()
            if 'tag_name' in data:
                return data['tag_name']

        return None

    def build_rpm(self, dir: str, owner: str, project: str) -> bool:
        cur_path = os.path.abspath(os.path.dirname(__file__))

        goreleaser_file = os.path.join(cur_path, f"goreleaser/{owner}/{project}/goreleaser.yml")
        if not os.path.isfile(goreleaser_file):
            logging.error(f"No goreleaser file defined for {owner}/{project}")
            return False

        goreleaser_args = ["goreleaser", "release", "--skip-validate", "--rm-dist", "-f", goreleaser_file]
        
        logging.info("Invoking running goreleaser with args '%s'", goreleaser_args[1:])
        p = subprocess.Popen(goreleaser_args, cwd=dir)
        p.wait()
        return True

    def build_rpms(self, repos: list):
        for repo in repos:
            tag = self.get_latest_tag(repo[0], repo[1])
            working_dir = self.checkout_latest_tag(repo[0], repo[1], tag)
            self.build_rpm(working_dir, repo[0], repo[1])


def setup_logging(debug=False):
    """ Sets up the logging. """
    loglevel = logging.INFO
    if debug:
        loglevel = logging.DEBUG
    logging.basicConfig(
        level=loglevel, format="%(levelname)s\t %(asctime)s %(message)s"
    )

def get_github_projects() -> list:
    projects = [
        ("prometheus", "blackbox_exporter"),
        ("prometheus", "alertmanager"),
        ("prometheus", "node_exporter"),
        ("metalmatze", "alertmanager-bot"),    
    ]

    return projects

if __name__ == "__main__":
    setup_logging()
    builder = RpmBuilder()
    repos = get_github_projects()
    builder.build_rpms(repos)